﻿To start the program, you need to call the method 
`print_lexical_analyzer_report(string file_name)` from the `PrettyPrinter` class.
As a parameter you need to pass the path to the test file.
All test files are named "input.sig". 
They are each in its own folder, which is located in the 'tests' folder.
The program creates a "generated.txt" file, which is located in the same directory as the test file.