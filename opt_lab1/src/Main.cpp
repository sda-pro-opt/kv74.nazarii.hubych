#include <string>
#include <iostream>
#include "..\include\PrettyPrinter.h"

int main() {
	PrettyPrinter printer;
	
	printer.print_lexical_analyzer_report("tests/true_test/input.sig");
	printer.print_lexical_analyzer_report("tests/false_test1/input.sig");
	printer.print_lexical_analyzer_report("tests/false_test2/input.sig");
	printer.print_lexical_analyzer_report("tests/false_test3/input.sig");
	printer.print_lexical_analyzer_report("tests/false_test4/input.sig");
	printer.print_lexical_analyzer_report("tests/false_test5/input.sig");

	system("pause");
}