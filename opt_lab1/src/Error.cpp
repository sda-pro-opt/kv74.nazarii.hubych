#include "..\include\Error.h"

Error::Error() { }

Error::Error(int line, int column, string message) {
	this->message.assign(message);
	this->line = line;
	this->column = column;
}

Error::~Error() {
}

string Error::to_string() {
	string output = "";

	if (this->line != 0 && this->column != 0) {
		output = "ERROR on line: " + std::to_string(this->line);
		output.append(", column: " + std::to_string(this->column));
		output.append("\n" + this->message + "\n\n");
	}

	return output;
}
