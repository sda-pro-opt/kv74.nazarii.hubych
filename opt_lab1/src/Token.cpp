#include "..\include\Token.h"

using namespace std;

Token::Token(){}

Token::Token(string token, string type, int line, int column, int code)
{
	this->token.assign(token);
	this->type.assign(type);
	this->line = line;
	this->column = column;
	this->code = code;
}

string Token::get_token()
{
	return this->token;
}

string Token::get_type()
{
	return this->type;
}

int Token::get_line()
{
	return this->line;
}

int Token::get_column() 
{
	return this->column;
}

int Token::get_code()
{
	return this->code;
}

Token::~Token()
{
}
