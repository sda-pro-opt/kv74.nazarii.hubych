#include "..\include\LexicalAnalyzer.h"

LexicalAnalyzer::LexicalAnalyzer() { }

LexicalAnalyzer::~LexicalAnalyzer() { }

vector<Token> LexicalAnalyzer::split_by_tokens(string file_name) {
	ifstream file(file_name);
	char symbol;

	while (file.get(symbol)) {
		line_symbol_count++;

		if (contains(single_delimiters, symbol)) {
			push_if_exists();
			string type = "single delimiter";

			if (is_assign(symbol, file)) {
				int column_number = line_symbol_count - 1;
				int code = 301;
				type = "multiple delimiter";

				tokens.push_back(*new Token(":=", type, line_number, column_number, code));
			}
			else if (is_comment(symbol, file)) {
				skip_comment(symbol, file);
			}
			else {
				tokens.push_back(*new Token(string(1, symbol), type, line_number, line_symbol_count, symbol));
			}
		}
		else if (contains(whitespace, symbol)) {
			push_if_exists();
		}
		else if (is_constant(symbol) || is_identifier(symbol)) {
			buffer.push_back(symbol);
		}
		else {
			create_error(line_number, line_symbol_count, "Wrong symbol: " + string(1, symbol));
			is_error = true;
		}

		check_assign(symbol);
		check_semicolon(symbol);

		if (is_error) {
			break;
		}

		change_counters_if_new_line(symbol);
	}
	check_dot();

	file.close();
	return tokens;
}

void LexicalAnalyzer::push_if_exists() {
	if (!buffer.empty()) {
		int column_number = line_symbol_count - buffer.length();
		int code = 0;
		string type;

		if (contains(keywords, buffer)) {
			keywords_code_counter++;
			code = keywords_code_counter;
			type = "keyword";
		}
		else if (is_constant(buffer[0])) {
			code = count_constant_code();
			type = "constant";
		}
		else if (is_identifier(buffer[0])) {
			code = count_identifier_code();
			type = "identifier";
		}

		tokens.push_back(*new Token(buffer, type, line_number, column_number, code));
		buffer.clear();
	}
}

int LexicalAnalyzer::count_constant_code() {
	if (constants_code.count(buffer) > 0) {
		return constants_code[buffer];
	}
	else {
		constants_code_counter++;
		constants_code.insert({ buffer, constants_code_counter });

		return constants_code_counter;
	}
}

int LexicalAnalyzer::count_identifier_code() {
	if (identifiers_code.count(buffer) > 0) {
		return identifiers_code[buffer];
	}
	else {
		identifiers_code_counter++;
		identifiers_code.insert({ buffer, identifiers_code_counter });

		return identifiers_code_counter;
	}
}

bool LexicalAnalyzer::is_assign(char symbol, ifstream& file) {
	if (symbol == ':') {
		file.get(symbol);
		line_symbol_count++;

		if (symbol == '=') {
			return true;
		}
		else {
			create_error(line_number, line_symbol_count, "Missing '='");
			is_error = true;
			return false;
		}
	}

	return false;
}

bool LexicalAnalyzer::is_comment(char symbol, ifstream& file) {
	if (symbol == '(') {
		file.get(symbol);
		if (symbol == '*') {
			return true;
		}
	}
	return false;
}

void LexicalAnalyzer::skip_comment(char symbol, ifstream& file) {
	bool is_closed = false;
	while (file.get(symbol)) {
		if (symbol == '*') {
			file.get(symbol);
			if (symbol == ')') {
				is_closed = true;
				break;
			}
		}
		change_counters_if_new_line(symbol);
	}

	if (!is_closed) {
		create_error(line_number, line_symbol_count, "Unclosed comment");
		is_error = true;
	}
}

bool LexicalAnalyzer::is_identifier(char symbol) {
	if (contains(letters, symbol)) {
		return true;
	}

	if (contains(digits, symbol) && contains(letters, buffer[0])) {
		return true;
	}

	return false;
}

bool LexicalAnalyzer::is_constant(char symbol) {
	if (contains(digits, symbol) && (contains(digits, buffer[0]) || buffer.empty())) {
		return true;
	}

	return false;
}

void LexicalAnalyzer::check_assign(char symbol) {
	if (!contains_token("BEGIN") && symbol == ':') {
		create_error(line_number, line_symbol_count, "Wrong symbol ':'\nAssign constant only with '='");
		is_error = true;
	}
	else if (contains_token("BEGIN") && tokens.back().get_token() == "=") {
		create_error(line_number, line_symbol_count, "Missing ':'\nAssign variable with ':='");
		is_error = true;
	}
}

void LexicalAnalyzer::check_semicolon(char symbol) {
	if (!tokens.empty() && is_constant(tokens.back().get_token()[0])) {
		if (symbol != ';') {
			create_error(line_number, line_symbol_count, "Missing ';'");
			is_error = true;
		}
	}
}

void LexicalAnalyzer::check_dot() {
	if (!is_error && tokens.back().get_token() != ".") {
		create_error(line_number, line_symbol_count, "Missing '.'");
		is_error = true;
	}
}

void LexicalAnalyzer::create_error(int line, int column, string message) {
	error = *new Error(line, column, message);
}

void LexicalAnalyzer::change_counters_if_new_line(char symbol) {
	if (symbol == '\n') {
		line_number++;
		line_symbol_count = 0;
	}
}

bool LexicalAnalyzer::contains(string delimeters, char symbol) {
	if (delimeters.find(symbol) != std::string::npos) {
		return true;
	}
	return false;
}

bool LexicalAnalyzer::contains(vector<string> data, string element) {
	return find(data.begin(), data.end(), element) != data.end();
}

bool LexicalAnalyzer::contains_token(string token) {
	for (std::size_t i = 0; i < tokens.size(); ++i) {
		if (tokens[i].get_token() == token) {
			return true;
		}
	}
	return false;
}

Error LexicalAnalyzer::get_error() {
	return error;
}