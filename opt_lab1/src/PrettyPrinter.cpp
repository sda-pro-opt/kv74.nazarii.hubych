#include "..\include\PrettyPrinter.h"

PrettyPrinter::PrettyPrinter() { }

PrettyPrinter::~PrettyPrinter() { }

void PrettyPrinter::print_lexical_analyzer_report(string file_name) {
	LexicalAnalyzer lexical_analyzer;
	PrettyPrinter printer;
	vector<Token> tokens = lexical_analyzer.split_by_tokens(file_name);

	string report_file_name = get_file_path(file_name) + "generated.txt";

	ofstream outfile(report_file_name);

	Error error = lexical_analyzer.get_error();
	outfile << error.to_string();

	print_tokens(tokens, outfile);
	outfile << "==============================================================\n";
	print_tokens_by_type(tokens, "keyword", outfile);
	print_tokens_by_type(tokens, "identifier", outfile);
	print_tokens_by_type(tokens, "constant", outfile);
	print_tokens_by_type(tokens, "single delimiter", outfile);
	print_tokens_by_type(tokens, "multiple delimiter", outfile);

	outfile.close();
}

void PrettyPrinter::print_file(string file_name) {
	ifstream file(file_name);
	string line;

	while (getline(file, line))
		cout << line << '\n';

	file.close();
}

void  PrettyPrinter::print_tokens_by_type(vector<Token> tokens, string type, ofstream &outfile) {
	outfile << "==> " << type << "s" << " <==" << endl;
	for (int i = 0; i < tokens.size(); i++) {
		if (tokens[i].get_type() == type) {
			outfile << "token: " << tokens.at(i).get_token() << endl;
			outfile << "line: " << tokens.at(i).get_line() << endl;
			outfile << "column: " << tokens.at(i).get_column() << ", ";
			outfile << "code: " << tokens.at(i).get_code() << ", ";
			outfile << "\n\n";
		}
	}
	outfile << endl;
}

void PrettyPrinter::print_tokens(vector<Token> tokens, ofstream &outfile) {
	for (int i = 0; i < tokens.size(); i++) {
		outfile << "token: " << tokens.at(i).get_token() << endl;
		outfile << "type: " << tokens.at(i).get_type() << endl;
		outfile << "line: " << tokens.at(i).get_line() << ", ";
		outfile << "column: " << tokens.at(i).get_column() << ", ";
		outfile << "code: " << tokens.at(i).get_code() << ", ";
		outfile << "\n\n";
	}
	outfile << endl;
}

string PrettyPrinter::get_file_path(string fullname) {
	size_t lastindex = fullname.find_last_of("/");
	return fullname.substr(0, (lastindex + 1));
}