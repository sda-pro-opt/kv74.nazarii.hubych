#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <algorithm>
#include <map>
#include "..\include\Token.h"
#include "..\include\Error.h"

using namespace std;

class LexicalAnalyzer
{
private:
	vector<string> keywords = { "PROGRAM", "BEGIN", "END", "CONST" };
	string digits = "0123456789";
	string letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	string single_delimiters = ";.=-:(*)";
	string whitespace = " \n\r\t";

	vector<Token> tokens;
	Error error;

	string buffer = "";
	int line_number = 1;
	int line_symbol_count = 0;

	int keywords_code_counter = 400;
	int constants_code_counter = 500;
	int identifiers_code_counter = 1000;

	map<string, int> constants_code;
	map<string, int> identifiers_code;

	bool contains(string delimeters, char symbol);
	bool contains(vector<string> data, string element);

	bool is_error = false;

	void push_if_exists();
	void change_counters_if_new_line(char symbol);

	bool is_assign(char symbol, ifstream& file);
	bool is_identifier(char symbol);
	bool is_constant(char symbol);
	bool is_comment(char symbol, ifstream& file);
	bool contains_token(string token);

	void skip_comment(char symbol, ifstream& file);
		
	int count_constant_code();
	int count_identifier_code();
	
	void create_error(int line, int column, string message);
	void check_semicolon(char symbol);
	void check_assign(char symbol);
	void check_dot();
public:
	LexicalAnalyzer();
	~LexicalAnalyzer();

	vector<Token> split_by_tokens(string file_name);
	Error get_error();
};