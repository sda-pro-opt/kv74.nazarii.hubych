#pragma once
#include <string>

using namespace std;

class Token
{
private:
	string token;
	string type;
	int line;
	int column;
	int code;

public:
	Token();
	Token(string token, string type, int line, int column, int code);
	~Token();
	
	string get_token();
	string get_type();
	int get_line();
	int get_column();
	int get_code();
};

