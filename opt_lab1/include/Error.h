#pragma once
#include <string>
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

class Error
{
private:
	string message;
	int line;
	int column;

public:
	Error();
	Error(int line, int column, string message);
	~Error();

	string to_string();
};
