#pragma once
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include "..\include\Token.h"
#include "..\include\LexicalAnalyzer.h"

using namespace std;

class PrettyPrinter
{
private:
	void print_file(string file_name);
	void print_tokens(vector<Token> tokens, ofstream &outfile);
	void print_tokens_by_type(vector<Token> tokens, string type, ofstream &outfile);
	string get_file_path(string fullname);
public:
	PrettyPrinter();
	~PrettyPrinter();

	void print_lexical_analyzer_report(string file_name);
};
